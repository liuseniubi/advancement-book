# Advancement Book
Adds an advancement book and advancement stand that opens your advancement menu. 

Downloads and more info can be found on [curse](https://www.curseforge.com/minecraft/mc-mods/advancement-book).

## Contributing

Everyone is welcome to contribute to this mod by creating a pull request or by creating and issue.