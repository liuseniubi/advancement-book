package net.parkermc.advancementbook;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.parkermc.advancementbook.items.AdvancementBookItems;

@Mod(AdvancementBookMod.MODID)
public class AdvancementBookMod{
    public static final String MODID = "advancementbook";
    public static ItemGroup tab = new ItemGroup("advancement_book") {
		
		@Override
		public ItemStack createIcon() {
			return new ItemStack(AdvancementBookItems.advancement_book);
		}
	};
    
    public AdvancementBookMod() {
        ModLoadingContext.get().registerConfig(net.minecraftforge.fml.config.ModConfig.Type.COMMON, AdvancementBookConfig.SPEC);
    }
}

