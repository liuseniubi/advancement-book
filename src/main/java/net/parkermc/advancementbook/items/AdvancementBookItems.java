package net.parkermc.advancementbook.items;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;
import net.parkermc.advancementbook.AdvancementBookMod;

@ObjectHolder(AdvancementBookMod.MODID)
@Mod.EventBusSubscriber(modid = AdvancementBookMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class AdvancementBookItems {
	
	@ObjectHolder("advancement_book")
	public static final AdvancementBookItem advancement_book = null;
	
	
	@SubscribeEvent
	public static void registerItemBlock(RegistryEvent.Register<Item> event) {
		Item.Properties advancementBookProps = new Item.Properties().group(AdvancementBookMod.tab).maxStackSize(1);
		event.getRegistry().registerAll(
				new AdvancementBookItem(advancementBookProps).setRegistryName(AdvancementBookMod.MODID, "advancement_book")
		);
	}
}
