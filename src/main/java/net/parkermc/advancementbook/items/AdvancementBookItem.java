package net.parkermc.advancementbook.items;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.advancements.AdvancementsScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;

public class AdvancementBookItem extends Item {
	
	public AdvancementBookItem(Item.Properties props) {
		super(props);
	}
		
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand hand) {
		if(worldIn.isRemote) {
			DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> { // Used this so we can return success server side
				openAdvancementScreen();
			});
		}
		return new ActionResult<ItemStack>(ActionResultType.SUCCESS, playerIn.getHeldItem(hand));
	}
	
	@OnlyIn(Dist.CLIENT)
	private void openAdvancementScreen() {
		Minecraft mc = Minecraft.getInstance();
		mc.displayGuiScreen(new AdvancementsScreen(mc.player.connection.getAdvancementManager()));
	}
	
}
