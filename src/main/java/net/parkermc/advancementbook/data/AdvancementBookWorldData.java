package net.parkermc.advancementbook.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.DimensionSavedDataManager;
import net.minecraft.world.storage.WorldSavedData;
import net.parkermc.advancementbook.AdvancementBookMod;

public class AdvancementBookWorldData extends WorldSavedData{
	private static final String DATA_NAME = AdvancementBookMod.MODID;
	
	public List<UUID> givenBook = new ArrayList<UUID>();
	
	
	private AdvancementBookWorldData() {
		super(DATA_NAME);
	}
	
	public static AdvancementBookWorldData get(ServerWorld world) {
		DimensionSavedDataManager dataManager = world.getSavedData();
		AdvancementBookWorldData instance = (AdvancementBookWorldData) dataManager.getOrCreate(AdvancementBookWorldData::new, DATA_NAME);

		if (instance == null) {
			instance = new AdvancementBookWorldData();
			dataManager.set(instance);
		  }
		
		  return instance;
	}
	
	@Override
	public void read(CompoundNBT nbt) {
		this.givenBook.clear();
		ListNBT nbtGivenBook = nbt.getList("givenBook", 8);
		for(int i=0;i < nbtGivenBook.size();i++) {
			this.givenBook.add(UUID.fromString(nbtGivenBook.getString(i)));
		}
	}
	
	@Override
	public CompoundNBT write(CompoundNBT compound) {
		ListNBT nbtGivenBook = new ListNBT();
		for(UUID uuid : this.givenBook) {
			nbtGivenBook.add(StringNBT.valueOf(uuid.toString()));
		}
		compound.put("givenBook", nbtGivenBook);
		return compound;
	}
}
