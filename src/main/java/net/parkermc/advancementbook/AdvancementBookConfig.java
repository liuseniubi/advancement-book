package net.parkermc.advancementbook;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig.Reloading;

import java.nio.file.Path;

@Mod.EventBusSubscriber
public class AdvancementBookConfig {

    public static final String CATEGORY_GENERAL = "general";
    public static ForgeConfigSpec.BooleanValue GIVE_FIRST_JOIN;
    
    private static final ForgeConfigSpec.Builder BUILDER = new ForgeConfigSpec.Builder();
    public static ForgeConfigSpec SPEC;


    static {

        BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        GIVE_FIRST_JOIN = BUILDER.comment("Enables giving everyone a book the first time they join").define("giveFirstJoin", true);
        BUILDER.pop();

        SPEC = BUILDER.build();
    }


    public static void loadConfig(ForgeConfigSpec spec, Path path) {

        final CommentedFileConfig configData = CommentedFileConfig.builder(path)
                .sync().autosave().writingMode(WritingMode.REPLACE).build();

        configData.load();
        spec.setConfig(configData);
    }

    @SubscribeEvent
    public static void onLoad(final net.minecraftforge.fml.config.ModConfig.Loading configEvent) {

    }

    @SubscribeEvent
    public static void onReload(final Reloading configEvent) {
    }


}
