package net.parkermc.advancementbook.tileentities;

import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.INameable;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

// This is an almost straight copy from the enchantment table tile entity 
public class AdvancementBookstandTileEntity extends TileEntity implements INameable, ITickableTileEntity {
   public int ticks;
   public float field_195523_f;
   public float field_195524_g;
   public float nextPageTurningSpeed;
   public float pageTurningSpeed;
   public float nextPageAngle;
   public float pageAngle;
   private float field_195525_h;
   private float field_195526_i;
   private float field_195531_n;
   private static final Random field_195532_o = new Random();
   private ITextComponent customname;

   public AdvancementBookstandTileEntity() {
      super(AdvancementBookTileEntities.advancement_bookstand_tile_entity);
   }
   
   @Override
   public CompoundNBT write(CompoundNBT compound) {
      super.write(compound);
      if (this.hasCustomName()) {
         compound.putString("CustomName", ITextComponent.Serializer.toJson(this.customname));
      }

      return compound;
   }

   @Override
   public void read(BlockState state, CompoundNBT compound) {
      super.read(state, compound);
      if (compound.contains("CustomName", 8)) {
         this.customname = ITextComponent.Serializer.getComponentFromJson(compound.getString("CustomName"));
      }

   }

   @Override
   public void tick() {
      this.pageTurningSpeed = this.nextPageTurningSpeed;
      this.pageAngle = this.nextPageAngle;
      PlayerEntity playerentity = this.world.getClosestPlayer((double)((float)this.pos.getX() + 0.5F), (double)((float)this.pos.getY() + 0.5F), (double)((float)this.pos.getZ() + 0.5F), 3.0D, false);
      if (playerentity != null) {
         double d0 = playerentity.getPosX() - (double)((float)this.pos.getX() + 0.5F);
         double d1 = playerentity.getPosZ() - (double)((float)this.pos.getZ() + 0.5F);
         this.field_195531_n = (float)MathHelper.atan2(d1, d0);
         this.nextPageTurningSpeed += 0.1F;
         if (this.nextPageTurningSpeed < 0.5F || field_195532_o.nextInt(40) == 0) {
            float f1 = this.field_195525_h;

            while(true) {
               this.field_195525_h += (float)(field_195532_o.nextInt(4) - field_195532_o.nextInt(4));
               if (f1 != this.field_195525_h) {
                  break;
               }
            }
         }
      } else {
         this.field_195531_n += 0.02F;
         this.nextPageTurningSpeed -= 0.1F;
      }

      while(this.nextPageAngle >= (float)Math.PI) {
         this.nextPageAngle -= ((float)Math.PI * 2F);
      }

      while(this.nextPageAngle < -(float)Math.PI) {
         this.nextPageAngle += ((float)Math.PI * 2F);
      }

      while(this.field_195531_n >= (float)Math.PI) {
         this.field_195531_n -= ((float)Math.PI * 2F);
      }

      while(this.field_195531_n < -(float)Math.PI) {
         this.field_195531_n += ((float)Math.PI * 2F);
      }

      float f2;
      for(f2 = this.field_195531_n - this.nextPageAngle; f2 >= (float)Math.PI; f2 -= ((float)Math.PI * 2F)) {
         ;
      }

      while(f2 < -(float)Math.PI) {
         f2 += ((float)Math.PI * 2F);
      }

      this.nextPageAngle += f2 * 0.4F;
      this.nextPageTurningSpeed = MathHelper.clamp(this.nextPageTurningSpeed, 0.0F, 1.0F);
      ++this.ticks;
      this.field_195524_g = this.field_195523_f;
      float f = (this.field_195525_h - this.field_195523_f) * 0.4F;
      f = MathHelper.clamp(f, -0.2F, 0.2F);
      this.field_195526_i += (f - this.field_195526_i) * 0.9F;
      this.field_195523_f += this.field_195526_i;
   }

   @Override
   public ITextComponent getName() {
      return (ITextComponent)(this.customname != null ? this.customname : new TranslationTextComponent("tile.advancement_bookstand.name"));
   }

   public void setCustomName(@Nullable ITextComponent name) {
      this.customname = name;
   }

   
   @Override
   @Nullable
   public ITextComponent getCustomName() {
      return this.customname;
   }
}