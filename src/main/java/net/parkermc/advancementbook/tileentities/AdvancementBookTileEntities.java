package net.parkermc.advancementbook.tileentities;

import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.registries.ObjectHolder;
import net.parkermc.advancementbook.AdvancementBookMod;
import net.parkermc.advancementbook.blocks.AdvancementBookBlocks;

@ObjectHolder(AdvancementBookMod.MODID)
@Mod.EventBusSubscriber(modid = AdvancementBookMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class AdvancementBookTileEntities {

	@ObjectHolder("advancement_bookstand_tile_entity")
	public static final TileEntityType<AdvancementBookstandTileEntity> advancement_bookstand_tile_entity = null;

	
	@SubscribeEvent
	public static void registerTileEntity(RegistryEvent.Register<TileEntityType<?>> evt) {
	  evt.getRegistry().registerAll(
			  TileEntityType.Builder.create(AdvancementBookstandTileEntity::new, AdvancementBookBlocks.advancement_bookstand).build(null).setRegistryName(AdvancementBookMod.MODID, "advancement_bookstand_tile_entity")
			  );
	}
	
	@SuppressWarnings("deprecation")
	@OnlyIn(Dist.CLIENT)
	@SubscribeEvent
	public static void onTextureStitchEvent(TextureStitchEvent.Pre event) {
		if (event.getMap().getTextureLocation().equals(AtlasTexture.LOCATION_BLOCKS_TEXTURE)){
			event.addSprite(AdvancementBookstandTileEntityRenderer.TEXTURE_BOOK_LOCATION);
		}
	}
	
	@SubscribeEvent
	@OnlyIn(Dist.CLIENT)
    public static void clientSetup(FMLClientSetupEvent event) {
		// Register the renderers
		ClientRegistry.bindTileEntityRenderer(advancement_bookstand_tile_entity, AdvancementBookstandTileEntityRenderer::new);
    }
}
