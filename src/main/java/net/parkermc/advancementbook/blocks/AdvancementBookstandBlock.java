package net.parkermc.advancementbook.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.ContainerBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.advancements.AdvancementsScreen;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.parkermc.advancementbook.tileentities.AdvancementBookstandTileEntity;

public class AdvancementBookstandBlock extends ContainerBlock {
   protected static final VoxelShape SHAPE = Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 12.0D, 16.0D);

   public AdvancementBookstandBlock(Block.Properties props) {
      super(props);
   }
   
   @Override
   public boolean isTransparent(BlockState state) { 
      return true;
   }

   @Override
   public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
      return SHAPE;
   }

   @Override
   public BlockRenderType getRenderType(BlockState state) {
      return BlockRenderType.MODEL;
   }

   @Override
   public TileEntity createNewTileEntity(IBlockReader worldIn) {
      return new AdvancementBookstandTileEntity();
   }

   @Override
   public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(worldIn.isRemote) {
			DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> { // Used this so we can return success server side
				openAdvancementScreen();
			});
		}
		return ActionResultType.SUCCESS;
   }

   @Override
   public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {
      if (stack.hasDisplayName()) {
         TileEntity tileentity = worldIn.getTileEntity(pos);
         if (tileentity instanceof AdvancementBookstandTileEntity) {
            ((AdvancementBookstandTileEntity)tileentity).setCustomName(stack.getDisplayName());
         }
      }
   }
   
	@OnlyIn(Dist.CLIENT)
	private void openAdvancementScreen() {
		Minecraft mc = Minecraft.getInstance();
		mc.displayGuiScreen(new AdvancementsScreen(mc.player.connection.getAdvancementManager()));
	}
   
}
