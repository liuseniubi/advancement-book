package net.parkermc.advancementbook.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;
import net.parkermc.advancementbook.AdvancementBookMod;

@ObjectHolder(AdvancementBookMod.MODID)
@Mod.EventBusSubscriber(modid = AdvancementBookMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class AdvancementBookBlocks {
	
	@ObjectHolder("advancement_bookstand")
	public static final AdvancementBookstandBlock advancement_bookstand = null;
	
	
	@SubscribeEvent
	public static void registerBlock(RegistryEvent.Register<Block> event) {
		Block.Properties bookstandProps = Block.Properties.create(Material.WOOD, MaterialColor.RED).hardnessAndResistance(1.5F, 5.0F);
		event.getRegistry().registerAll(
				new AdvancementBookstandBlock(bookstandProps).setRegistryName(AdvancementBookMod.MODID, "advancement_bookstand")
		);
	}
	
	@SubscribeEvent
	public static void registerItemBlock(RegistryEvent.Register<Item> event) {
		Item.Properties bookstandProps = new Item.Properties().group(AdvancementBookMod.tab);
		
		event.getRegistry().registerAll(
				new BlockItem(advancement_bookstand, bookstandProps).setRegistryName(AdvancementBookMod.MODID, "advancement_bookstand")
		);
	}
}
