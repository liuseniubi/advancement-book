package net.parkermc.advancementbook.events;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.parkermc.advancementbook.AdvancementBookMod;
import net.parkermc.advancementbook.AdvancementBookConfig;
import net.parkermc.advancementbook.data.AdvancementBookWorldData;
import net.parkermc.advancementbook.items.AdvancementBookItems;

@Mod.EventBusSubscriber(modid = AdvancementBookMod.MODID)
public class EventHandler {

	@SubscribeEvent
	public static void joinWorldEvent(EntityJoinWorldEvent event) {
		if(AdvancementBookConfig.GIVE_FIRST_JOIN.get() && (!event.getWorld().isRemote) && event.getEntity() instanceof PlayerEntity) {
			AdvancementBookWorldData data = AdvancementBookWorldData.get((ServerWorld)event.getWorld());
			if(!data.givenBook.contains(event.getEntity().getUniqueID())) {
				((PlayerEntity)event.getEntity()).inventory.addItemStackToInventory(new ItemStack(AdvancementBookItems.advancement_book, 1));
				data.givenBook.add(event.getEntity().getUniqueID());
				data.markDirty();
			}
		}
	}
}
